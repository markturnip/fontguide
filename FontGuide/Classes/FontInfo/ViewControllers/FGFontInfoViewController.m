//
//  FGFontInfoViewController.m
//  FontGuide
//
//  Created by Mark Turner on 15/05/2013.
//  Copyright (c) 2013 Mark Turner. All rights reserved.
//

#import "FGFontInfoViewController.h"
#import "FGFontFamily.h"

@interface FGFontInfoViewController () {
    UIFont * _currentFont;
}

@end

@implementation FGFontInfoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background_tile" ]]];
    [self.navigationItem setTitle:self.fontFamily.name];
    
    //Generate alphabet string. 
    NSMutableString * alphabetString = [NSMutableString new];
    for (char c = 'A'; c <= 'Z'; c++) {
        [alphabetString appendFormat:@"%c",c];
    }
    
    _currentFont = [UIFont fontWithName:self.fontFamily.name size:50.0f];
    [self.fontLabel setFont:_currentFont];
    [self.fontLabel setText:alphabetString];
    
    [self.fontFamily setFrequency:[NSNumber numberWithInteger:self.fontFamily.frequency.integerValue+1]];
    
    [self.fontSizeSlider setValue:0.0f];
    [self updateFontSize:self.fontSizeSlider.value];
    
//    NSMutableParagraphStyle * paragraphStyle = nil;
//    paragraphStyle = [[NSMutableParagraphStyle alloc] init];
//    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
//    paragraphStyle.alignment     = NSTextAlignmentCenter;
//    paragraphStyle.maximumLineHeight = 30.0f;
//
//    NSDictionary * attibuteDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
//                                         paragraphStyle, NSParagraphStyleAttributeName,
//                                         [UIColor whiteColor], NSForegroundColorAttributeName,
//                                         [NSNumber numberWithInteger:NSUnderlineStyleSingle],
//                                         NSStrikethroughStyleAttributeName,
//                                         font,NSFontAttributeName,nil];
//    
//    NSAttributedString * attributedString = [[NSAttributedString alloc] initWithString:alphabetString
//                                                                            attributes:attibuteDictionary];
//    [self.fontLabel setAttributedText:attributedString];
    
    [self.ratingSlider setValue:self.fontFamily.rating.floatValue];
}

- (IBAction)fontSizeSliderValueChanged:(UISlider *)sender
{
    [self updateFontSize:sender.value];
}

- (IBAction)ratingSliderValueChanged:(UISlider *)sender
{
    [self.fontFamily setRating:[NSNumber numberWithInteger:ceil(sender.value*5.0)]];
}

- (void)updateFontSize:(CGFloat)size
{
    _currentFont = [_currentFont fontWithSize:20.0+(size*100)];
    [self.fontLabel setFont:_currentFont];
}

@end
