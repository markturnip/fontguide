//
//  FGFontInfoViewController.h
//  FontGuide
//
//  Created by Mark Turner on 15/05/2013.
//  Copyright (c) 2013 Mark Turner. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FGFontFamily;
@interface FGFontInfoViewController : UIViewController

@property (nonatomic,strong) FGFontFamily * fontFamily;

@property (weak, nonatomic) IBOutlet UILabel *fontLabel;
@property (weak, nonatomic) IBOutlet UISlider *ratingSlider;
@property (weak, nonatomic) IBOutlet UISlider *fontSizeSlider;

- (IBAction)fontSizeSliderValueChanged:(id)sender;
- (IBAction)ratingSliderValueChanged:(UISlider *)sender;

@end
