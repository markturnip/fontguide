//
//  FGFont.h
//  FontGuide
//
//  Created by Mark Turner on 15/05/2013.
//  Copyright (c) 2013 Mark Turner. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class FGFontFamily;

@interface FGFont : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) FGFontFamily *family;

@end
