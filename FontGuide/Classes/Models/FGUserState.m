//
//  FGUserState.m
//  FontGuide
//
//  Created by Mark Turner on 15/05/2013.
//  Copyright (c) 2013 Mark Turner. All rights reserved.
//

#import "FGUserState.h"
#import "FGFontFamily.h"


@implementation FGUserState

@dynamic layoutState;
@dynamic layoutBackwards;
@dynamic reverseSort;
@dynamic sortingState;
@dynamic fontFamilies;

@end
