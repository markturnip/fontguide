//
//  FGFontFamily.h
//  FontGuide
//
//  Created by Mark Turner on 17/05/2013.
//  Copyright (c) 2013 Mark Turner. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class FGFont, FGUserState;

@interface FGFontFamily : NSManagedObject

@property (nonatomic, retain) NSNumber * frequency;
@property (nonatomic, retain) NSNumber * index;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * rating;
@property (nonatomic, retain) NSNumber * size;
@property (nonatomic, retain) NSNumber * width;
@property (nonatomic, retain) NSNumber * nameLength;
@property (nonatomic, retain) NSSet *fonts;
@property (nonatomic, retain) FGUserState *parent;
@end

@interface FGFontFamily (CoreDataGeneratedAccessors)

- (void)addFontsObject:(FGFont *)value;
- (void)removeFontsObject:(FGFont *)value;
- (void)addFonts:(NSSet *)values;
- (void)removeFonts:(NSSet *)values;

@end
