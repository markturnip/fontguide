//
//  FGFontFamily.m
//  FontGuide
//
//  Created by Mark Turner on 17/05/2013.
//  Copyright (c) 2013 Mark Turner. All rights reserved.
//

#import "FGFontFamily.h"
#import "FGFont.h"
#import "FGUserState.h"


@implementation FGFontFamily

@dynamic frequency;
@dynamic index;
@dynamic name;
@dynamic rating;
@dynamic size;
@dynamic width;
@dynamic nameLength;
@dynamic fonts;
@dynamic parent;

@end
