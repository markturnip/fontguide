//
//  FGUserState.h
//  FontGuide
//
//  Created by Mark Turner on 15/05/2013.
//  Copyright (c) 2013 Mark Turner. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class FGFontFamily;

@interface FGUserState : NSManagedObject

@property (nonatomic, retain) NSNumber * layoutState;
@property (nonatomic, retain) NSNumber * layoutBackwards;
@property (nonatomic, retain) NSNumber * reverseSort;
@property (nonatomic, retain) NSNumber * sortingState;
@property (nonatomic, retain) NSSet *fontFamilies;
@end

@interface FGUserState (CoreDataGeneratedAccessors)

- (void)addFontFamiliesObject:(FGFontFamily *)value;
- (void)removeFontFamiliesObject:(FGFontFamily *)value;
- (void)addFontFamilies:(NSSet *)values;
- (void)removeFontFamilies:(NSSet *)values;

@end
