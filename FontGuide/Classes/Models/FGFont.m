//
//  FGFont.m
//  FontGuide
//
//  Created by Mark Turner on 15/05/2013.
//  Copyright (c) 2013 Mark Turner. All rights reserved.
//

#import "FGFont.h"
#import "FGFontFamily.h"


@implementation FGFont

@dynamic name;
@dynamic family;

@end
