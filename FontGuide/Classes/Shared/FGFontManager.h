//
//  FGFontManager.h
//  FontGuide
//
//  Created by Mark Turner on 15/05/2013.
//  Copyright (c) 2013 Mark Turner. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

typedef enum {
    FGFontFamilySortAlpha, //the library is sorted alphanumerically
    FGFontFamilySortCount, //the library is sorted by name length
    FGFontFamilySortSize,  //the library is sorted by display width of the name.
    FGFontFamilySortManual,  //the library is sorted manually
} FGFontFamilySortState;

@class FGUserState;
@interface FGFontManager : NSObject
@property (nonatomic,readonly) NSManagedObjectContext * managedContext;

+ (FGFontManager *)sharedFontManager;
- (FGUserState *)userState;
- (BOOL)saveState;
- (BOOL)reset;

- (void)setLibrarySort:(FGFontFamilySortState)state ascending:(BOOL)ascending;
@end
