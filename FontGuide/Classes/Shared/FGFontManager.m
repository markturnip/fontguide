//
//  FGFontManager.m
//  FontGuide
//
//  Created by Mark Turner on 15/05/2013.
//  Copyright (c) 2013 Mark Turner. All rights reserved.
//

#import "FGFontManager.h"

#import "FGUserState.h"
#import "FGFontFamily.h"
#import "FGFont.h"

static NSString *const kUserStateKey = @"FGUserState";

@implementation FGFontManager {
    NSManagedObjectContext       * _managedContext;
    NSPersistentStoreCoordinator * _persistentStoreCoordinator;
    
    FGUserState * _userState;
}

#pragma mark - 

+ (FGFontManager *)sharedFontManager
{
    static dispatch_once_t pred;
    static FGFontManager *shared = nil;
    dispatch_once(&pred, ^{
        shared = [[FGFontManager alloc] init];
    });
    return shared;
}

- (id)init
{
    self = [super init];
    if (self) {
        if (![[NSFileManager defaultManager] fileExistsAtPath:[[self class] dataStorePath]])
            [self reset];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(applicationWillResignActive:)
                                                     name:UIApplicationWillResignActiveNotification
                                                   object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)applicationWillResignActive:(NSNotification *)notification
{
    [self saveState];
}

#pragma mark - ResetState

- (BOOL)reset
{
    BOOL success = TRUE;
    
    _userState = nil;
    _managedContext = nil;
    _persistentStoreCoordinator = nil;
    
    NSString * dataStorePath  = [[self class] dataStorePath];
    
    //Remove existing data store.
    NSFileManager * fm = [NSFileManager defaultManager];
    if ([fm fileExistsAtPath:dataStorePath]) {
        NSError * deleteError = nil;
        if ([fm removeItemAtPath:dataStorePath error:&deleteError] == NO) {
            NSLog(@"Delete store error: %@",deleteError);
            success = FALSE;
        }
    }
    
    //Setup FGFontFamily objects. 
    NSArray * fontFamilyNamesArray = [UIFont familyNames];
    for (NSString * fontFamilyName in fontFamilyNamesArray) {
        FGFontFamily * fontFamily = [NSEntityDescription
                                     insertNewObjectForEntityForName:@"FontFamily"
                                     inManagedObjectContext:[self managedContext]];
        [fontFamily setName:fontFamilyName];
        [fontFamily setFrequency:[NSNumber numberWithInt:0]];
        [fontFamily setRating:[NSNumber numberWithInt:0]];

        NSArray * familyFontsArray = [UIFont fontNamesForFamilyName:fontFamily.name];
        for (NSString * fontNameString in familyFontsArray) {
            FGFont * fontManagedObject = [NSEntityDescription
                                          insertNewObjectForEntityForName:@"Font"
                                          inManagedObjectContext:[self managedContext]];
            [fontManagedObject setName:fontNameString];
            [fontFamily addFontsObject:fontManagedObject];
        }
    }
    
    //Fetch added data from context
    NSFetchRequest * fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"FontFamily" inManagedObjectContext:[self managedContext]]];

    NSError *fetchError = nil;
    NSArray *fetchedObjects = [self.managedContext executeFetchRequest:fetchRequest error:&fetchError];
    
    const CGFloat kCellLabelWidth = 269;
    
    //Configure font family objects. 
    if ((fetchedObjects != nil) && (fetchedObjects.count > 0))
    {
        //Figure out max font.
        CGFloat currentMax = 0.0;
        CGFloat i          = currentMax;
        while (i < 100.0f) {
            currentMax ++;
            i = currentMax;
            
            CGFloat actualFontSize = 0.0f;
            for (FGFontFamily * family in fetchedObjects) {
                actualFontSize = 0.0f;
                [family.name sizeWithFont:[UIFont fontWithName:family.name size:currentMax]
                              minFontSize:0
                           actualFontSize:&actualFontSize
                                 forWidth:kCellLabelWidth
                            lineBreakMode:NSLineBreakByWordWrapping];
                if (actualFontSize < currentMax)
                    currentMax = actualFontSize;
            }
            if (currentMax < i)
                break;
        }
        
        //Add all the font families to the user state.
        FGUserState * userState = [self userState];
        if (userState != nil) {
            for (FGFontFamily * family in fetchedObjects) {
                //NSSortDescriptor would not work with @"name.length" key.
                [family setNameLength:[NSNumber numberWithUnsignedInteger:family.name.length]];
                [family setSize:[NSNumber numberWithFloat:currentMax]];
                
                CGSize size = [family.name sizeWithFont:[UIFont fontWithName:family.name size:family.size.integerValue]
                                      constrainedToSize:CGSizeMake(kCellLabelWidth, 44.0)];
                [family setWidth:[NSNumber numberWithFloat:size.width]];
                [userState addFontFamiliesObject:family];
            }
        }
        
        //Alphanumerically
        [self setLibrarySort:FGFontFamilySortAlpha ascending:YES];
    } else {
        NSLog(@"Fetch error: %@",fetchError);
        success = FALSE;
    }
    
    success = [self saveState]?TRUE:FALSE;
    
    return success;
}

- (FGUserState *)userState
{
    if (_userState == nil) {
        //Check if user state URI exists.
        NSData * bbUserURI = [[NSUserDefaults standardUserDefaults] objectForKey:kUserStateKey];
        if (bbUserURI != nil) {
            NSURL *uri = [NSKeyedUnarchiver unarchiveObjectWithData:bbUserURI];
            NSManagedObject * managedObject = [self objectWithURI:uri];
            if ([managedObject isKindOfClass:[FGUserState class]])
                _userState = (FGUserState *)managedObject;
            
        }
        if (_userState == nil) {
            //Failed to fetch user state - Create new user state.
            FGUserState * userState = [NSEntityDescription
                                       insertNewObjectForEntityForName:@"UserState"
                                       inManagedObjectContext:[self managedContext]];
            [userState setLayoutState:[NSNumber numberWithInt:0]];
            [userState setLayoutBackwards:[NSNumber numberWithInt:0]];

            NSError * saveError = nil;
            if (![[self managedContext] save:&saveError])
                NSLog(@"Save error: %@",saveError);
            else {
                NSURL * userStateURI = [[userState objectID] URIRepresentation];
                NSData *uriData = [NSKeyedArchiver archivedDataWithRootObject:userStateURI];
                if (userStateURI != nil) {
                    [[NSUserDefaults standardUserDefaults] setObject:uriData forKey:kUserStateKey];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
                _userState = userState;
            }
        }
    }
    return _userState;
}

- (BOOL)saveState
{
    BOOL saveSuccess = TRUE;
    NSError * saveError = nil;

    if ([[self managedContext] save:&saveError])
        saveSuccess = TRUE;
    else
        NSLog(@"Save error:%@",saveError);

    return saveSuccess;
}
    
- (void)setLibrarySort:(FGFontFamilySortState)state ascending:(BOOL)ascending
{
    [_userState setSortingState:[NSNumber numberWithInteger:state]];
    [_userState setReverseSort:[NSNumber numberWithBool:ascending]];
    
    NSFetchRequest * fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"FontFamily" inManagedObjectContext:[self managedContext]]];

    NSError *fetchError     = nil;
    NSArray *fetchedObjects = nil;
    
    switch (state) {
        case FGFontFamilySortAlpha: {
            fetchedObjects = [self.managedContext executeFetchRequest:fetchRequest error:&fetchError];
            //Alphanumuric order
            fetchedObjects = [fetchedObjects sortedArrayUsingComparator:^NSComparisonResult(FGFontFamily * obj1, FGFontFamily * obj2) {
                if (_userState.reverseSort.boolValue) {
                    return [obj1.name compare:obj2.name options:NSNumericSearch];
                } else {
                    return [obj2.name compare:obj1.name options:NSNumericSearch];
                }
            }];
        }
            break;
        case FGFontFamilySortCount: {
            NSSortDescriptor * sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"nameLength" ascending:_userState.reverseSort.boolValue];
            fetchRequest.sortDescriptors = @[sortDescriptor];
            fetchedObjects = [self.managedContext executeFetchRequest:fetchRequest error:&fetchError];
        }
            break;
        case FGFontFamilySortSize: {
            NSSortDescriptor * sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"width" ascending:_userState.reverseSort.boolValue];
            fetchRequest.sortDescriptors = @[sortDescriptor];
            fetchedObjects = [self.managedContext executeFetchRequest:fetchRequest error:&fetchError];
        }
            break;
        case FGFontFamilySortManual: {
            NSSortDescriptor * sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"index" ascending:_userState.reverseSort.boolValue];
            fetchRequest.sortDescriptors = @[sortDescriptor];
            fetchedObjects = [self.managedContext executeFetchRequest:fetchRequest error:&fetchError];
            fetchedObjects = [[fetchedObjects reverseObjectEnumerator] allObjects];
        }
            break;
        default:
            break;
    }
    NSUInteger indexCount = 0;
    for (FGFontFamily * family in fetchedObjects)
        [family setIndex:[NSNumber numberWithUnsignedInt:indexCount ++]];
    [self saveState];
}

#pragma mark - CoreData

+ (NSString *)dataStorePath
{
    NSString * documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    return [documentsDirectory stringByAppendingPathComponent:@"FontGuide_store.sqlite"];;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator == nil) {
        NSError * error = nil;
        NSURL   * dataStorePathURL = [NSURL fileURLWithPath:[[self class] dataStorePath]];
        NSManagedObjectModel * managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];

        _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:managedObjectModel];
        if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                       configuration:nil
                                                                 URL:dataStorePathURL
                                                             options:nil
                                                               error:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            
            [self reset];
        }
    }
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)managedContext
{
    if (_managedContext == nil) {
         _managedContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_managedContext setPersistentStoreCoordinator:[self persistentStoreCoordinator]];
    }
    return _managedContext;
}

#pragma mark - CD Fetches

- (NSManagedObject *)objectWithURI:(NSURL *)uri
{
    NSManagedObject * object = nil;
    
    NSManagedObjectID *objectID = [[self persistentStoreCoordinator] managedObjectIDForURIRepresentation:uri];
    if (objectID == nil)
        return nil;
    
    NSManagedObject *objectForID = [[self managedContext] objectWithID:objectID];
    if ([objectForID isFault] == NO)
        return objectForID;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[objectID entity]];
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:[NSExpression expressionForEvaluatedObject]
                                                                rightExpression:[NSExpression expressionForConstantValue:objectForID]
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSEqualToPredicateOperatorType
                                                                        options:0];
    [request setPredicate:predicate];
    
    NSError * fetchError = nil;
    NSArray *results = [[self managedContext] executeFetchRequest:request error:&fetchError];
    if ([results count] > 0)
        object = [results objectAtIndex:0];
    
    return object;
}

@end
