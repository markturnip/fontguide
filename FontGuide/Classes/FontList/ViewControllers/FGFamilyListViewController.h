//
//  FGFamilyListViewController.h
//  FontGuide
//
//  Created by Mark Turner on 15/05/2013.
//  Copyright (c) 2013 Mark Turner. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FGFamilyListViewController : UIViewController
<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UISearchDisplayDelegate>

@property (nonatomic,weak) IBOutlet UITableView * tableView;

@end
