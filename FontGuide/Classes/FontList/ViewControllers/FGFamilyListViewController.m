//
//  FGFamilyListViewController.m
//  FontGuide
//
//  Created by Mark Turner on 15/05/2013.
//  Copyright (c) 2013 Mark Turner. All rights reserved.
//

#import "FGFamilyListViewController.h"
#import "FGFontManager.h"
#import "FGFontFamily.h"
#import "FGUserState.h"
#import "FGFontFamilyCell.h"
#import "FGFontInfoViewController.h"

#import "FGAppDelegate.h"
#import "FGMenuView.h"

#import "DDMenuController.h"
#import "SVSegmentedControl.h"

@interface FGFamilyListViewController () <NSFetchedResultsControllerDelegate>
{
    NSFetchedResultsController * _fontsFamilyFetchedResultsController;

    DDMenuController * _menuViewController;
    FGMenuView       * _menuView;
    
    BOOL               _userIsChanging;
    BOOL               _updatingViewState;
}
@end

@implementation FGFamilyListViewController

- (void)loadView
{
    [super loadView];

    NSArray * menuViewArray = [[NSBundle mainBundle] loadNibNamed:@"FGMenuView" owner:nil options:nil];
    if (menuViewArray.count == 1)
        _menuView = [menuViewArray objectAtIndex:0];
    
    [self.tableView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background_tile" ]]];
    [self.tableView registerNib:[UINib nibWithNibName:@"FGFontFamilyCell" bundle:nil] forCellReuseIdentifier:@"FontFamilyCell"];
    [self.tableView setContentInset:UIEdgeInsetsMake(10, 0, 0, 0)];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //init fetched results
     [self fetchedResults];
    [[self fetchedResults] setDelegate:self];
    
    _menuViewController = (DDMenuController*)((FGAppDelegate*)[[UIApplication sharedApplication] delegate]).menuController;
    UIViewController * vc = [[UIViewController alloc] init];
    [vc.view addSubview:_menuView];
    
    [_menuView.refreshButton addTarget:self
                                action:@selector(_reloadButtonTouchedUpInside:)
                      forControlEvents:UIControlEventTouchUpInside];
    
    [_menuView.layoutSegmentedControl addTarget:self
                                         action:@selector(_layoutSegmentedControlDidChangeValue:)
                               forControlEvents:UIControlEventValueChanged];
    [_menuView.reverseLayoutSwitch addTarget:self
                                      action:@selector(_reverseLayoutSwitchDidChangeValue:)
                            forControlEvents:UIControlEventValueChanged];
    [_menuView.sortSegmentedControl addTarget:self
                                       action:@selector(_sortSegmentedControlDidChangeValue:)
                             forControlEvents:UIControlEventValueChanged];
    [_menuView.sortReverseSwitch addTarget:self
                                    action:@selector(_sortReverseSwitchDidChangeValue:)
                          forControlEvents:UIControlEventValueChanged];
    
    [_menuViewController setLeftViewController:vc];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self _updateView];
    
    [self.navigationItem setRightBarButtonItem:self.editButtonItem];
    [self.editButtonItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                 [UIColor blackColor],UITextAttributeTextColor,
                                                 [UIColor clearColor],UITextAttributeTextShadowColor,nil]
                                       forState:UIControlStateNormal];
    
    UIImageView * titleView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 105, 28)];
    [titleView setContentMode:UIViewContentModeTopLeft];
    [titleView setImage:[UIImage imageNamed:@"nav_title"]];
    [self.navigationItem setTitleView:titleView];
}


- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    if (!editing) {
        [self _saveUserState];

        _userIsChanging = FALSE;
    }
    [self.tableView setEditing:editing animated:animated];
}

#pragma mark - Private Methods

- (void)_updateView
{
    FGUserState * userState = [[FGFontManager sharedFontManager] userState];

    _updatingViewState = TRUE;
    [_menuView setUserState:userState];
    _updatingViewState = FALSE;
}

- (void)_layoutSegmentedControlDidChangeValue:(SVSegmentedControl *)sender
{
    //Issue - SVSegmentedControl calls on target actions when is changed manually, which in turn causes table update. 
    if (_updatingViewState) return;

    FGFontFamilyCellLayoutState state = FGFontFamilyCellLayoutJustifiedLeft;
    switch (sender.selectedSegmentIndex) {
        case 0:
            state = FGFontFamilyCellLayoutJustifiedLeft;
            break;
        case 1:
            state = FGFontFamilyCellLayoutJustifiedRight;
            break;
        default:
            break;
    }
    [self setFontFamilyLayoutState:state];
}

- (void)_reverseLayoutSwitchDidChangeValue:(SVSegmentedControl *)sender
{
    //Issue - SVSegmentedControl calls on target actions when is changed manually, which in turn causes table update.
    if (_updatingViewState) return;

    [self setFontFamilyLayoutBackwardsState:sender.selectedSegmentIndex];
}

- (void)_sortSegmentedControlDidChangeValue:(SVSegmentedControl *)sender
{
    //Issue - SVSegmentedControl calls on target actions when is changed manually, which in turn causes table update.
    if (_updatingViewState) return;

    [self updateSortState];
}

- (void)_sortReverseSwitchDidChangeValue:(SVSegmentedControl *)sender
{
    //Issue - SVSegmentedControl calls on target actions when is changed manually, which in turn causes table update.
    if (_updatingViewState) return;

    [self updateSortState];
}

- (void)_reloadButtonTouchedUpInside:(id)sender
{
    _fontsFamilyFetchedResultsController = nil;
    [[FGFontManager sharedFontManager] reset];
    [self.tableView reloadData];
    
    [self _updateView];
    
    [_menuViewController showRootController:TRUE];
}

#pragma mark -

- (NSFetchedResultsController *)fetchedResults
{
    if (_fontsFamilyFetchedResultsController == nil) {
        NSManagedObjectContext * managedContext = [FGFontManager sharedFontManager].managedContext;
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"FontFamily"
                                                  inManagedObjectContext:managedContext];
        
        NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"index" ascending:YES];
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        [fetchRequest setEntity:entity];
        [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
    
        _fontsFamilyFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                   managedObjectContext:managedContext
                                                                                     sectionNameKeyPath:@"self.name"
                                                                                              cacheName:nil];
        NSError * fetchError = nil;
        [_fontsFamilyFetchedResultsController performFetch:&fetchError];
    }
    return _fontsFamilyFetchedResultsController;
}

- (void)_saveUserState
{
    [[FGFontManager sharedFontManager] saveState];
    [self _updateView];
}

- (void)_reloadVisibleCells
{
    [self.tableView reloadData];
//    [self.tableView reloadRowsAtIndexPaths:[self.tableView visibleCells] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Content Filtering

- (void)_filterFontFamilyWithSearch:(NSString*)searchString
{
    NSPredicate * predicate = nil;
    if (searchString != nil) {
        predicate = [NSPredicate predicateWithFormat:@"name BEGINSWITH[cd] %@", searchString];
    }
    [[[self fetchedResults] fetchRequest] setPredicate:predicate];
    
    NSError * fetchError = nil;
    if (![[self fetchedResults] performFetch:&fetchError])
        NSLog(@"Unresolved error %@", fetchError);
}

#pragma mark -

- (void)setFontFamilyLayoutState:(FGFontFamilyCellLayoutState)state
{
    FGUserState * userState = [[FGFontManager sharedFontManager] userState];
    [userState setLayoutState:[NSNumber numberWithUnsignedInteger:state]];
    [self _reloadVisibleCells];
    [_menuViewController showRootController:TRUE];
}

- (void)setFontFamilyLayoutBackwardsState:(BOOL)state
{
    FGUserState * userState = [[FGFontManager sharedFontManager] userState];
    [userState setLayoutBackwards:[NSNumber numberWithBool:state]];
    [self _reloadVisibleCells];
    [_menuViewController showRootController:TRUE];
}

- (void)updateSortState
{
    FGFontFamilySortState state = FGFontFamilySortManual;
    switch (_menuView.sortSegmentedControl.selectedSegmentIndex) {
        case 0:
            state = FGFontFamilySortAlpha;
            break;
        case 1:
            state = FGFontFamilySortCount;
            break;
        case 2:
            state = FGFontFamilySortSize;
            break;
        default:
            break;
    }
    
    [[FGFontManager sharedFontManager] setLibrarySort:state ascending:(_menuView.sortReverseSwitch.selectedSegmentIndex>=1?FALSE:TRUE)];
    [[FGFontManager sharedFontManager] saveState];
    
    [_menuViewController showRootController:TRUE];
    
    [self.tableView reloadSectionIndexTitles];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[[self fetchedResults] sections] count];
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[[self fetchedResults] sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FGFontFamilyCell * cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        //Required for search controller
        cell = [[[NSBundle mainBundle] loadNibNamed:@"FGFontFamilyCell" owner:nil options:nil] objectAtIndex:0];
    }
    [cell setFontFamily:[[self fetchedResults] objectAtIndexPath:indexPath]];
    return cell;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    FGUserState * userState = [[FGFontManager sharedFontManager] userState];
    if (userState.sortingState.integerValue == FGFontFamilySortAlpha) {
        return [[self fetchedResults] sectionIndexTitles];
    }
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    return [[self fetchedResults] sectionForSectionIndexTitle:title atIndex:index];
}

#pragma mark - 

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    FGFontFamily * fontFamily = [[self fetchedResults] objectAtIndexPath:indexPath];
    [self performSegueWithIdentifier:@"FontInfo" sender:fontFamily];
    [tableView deselectRowAtIndexPath:indexPath animated:TRUE];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath;
{
    _userIsChanging = TRUE;
    
    [[[FGFontManager sharedFontManager] userState] setSortingState:[NSNumber numberWithInteger:FGFontFamilySortManual]];
    [self _updateView];
    
    FGFontFamily * fontFamily = [[self fetchedResults] objectAtIndexPath:sourceIndexPath];

    NSMutableArray * newOrderArray = [[[self fetchedResults] fetchedObjects] mutableCopy];
    [newOrderArray removeObject:fontFamily];
    [newOrderArray insertObject:fontFamily atIndex:[destinationIndexPath row]];

    //Update indexes
    NSUInteger familyCount = 0;
    for (FGFontFamily * family in newOrderArray)
        [family setIndex:[NSNumber numberWithUnsignedInt:familyCount++]];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (editingStyle) {
        case UITableViewCellEditingStyleInsert:
            break;
        case UITableViewCellEditingStyleDelete: {
            FGUserState * userState = [[FGFontManager sharedFontManager] userState];
            [userState.managedObjectContext deleteObject:[[self fetchedResults] objectAtIndexPath:indexPath]];
        }
            break;
        case UITableViewCellEditingStyleNone:
            break;
            
        default:
            break;
    }
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    if (_userIsChanging) return;
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller
  didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex
     forChangeType:(NSFetchedResultsChangeType)type
{
    if (_userIsChanging) return;
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controller:(NSFetchedResultsController *)controller
   didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath
     forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    if (_userIsChanging) return;

    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeUpdate: {
            FGFontFamilyCell * cell = (FGFontFamilyCell*)[tableView cellForRowAtIndexPath:indexPath];
            if ([anObject isKindOfClass:[FGFontFamily class]]) {
                FGFontFamily * fontFamily = anObject;
                [cell setFontFamily:fontFamily];
            } else {
                NSLog(@"Not hot");
            }
        }
            break;
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray
                                               arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray
                                               arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    if (_userIsChanging) return;

    [self.tableView endUpdates];
}

#pragma mark - UISearchDisplayControllerDelegate

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self _filterFontFamilyWithSearch:[self.searchDisplayController.searchBar text]];
    return YES;
}

- (void)searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller
{
    [self _filterFontFamilyWithSearch:nil];
}

#pragma mark - Prepare segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [super prepareForSegue:segue sender:sender];
    if ([segue.destinationViewController isKindOfClass:[FGFontInfoViewController class]]) {
        FGFontInfoViewController * fontInfoVC = (FGFontInfoViewController*)segue.destinationViewController;
        if ((sender != nil) && ([sender isKindOfClass:[FGFontFamily class]])){
            [fontInfoVC setFontFamily:sender];
        }
    }
}

@end
