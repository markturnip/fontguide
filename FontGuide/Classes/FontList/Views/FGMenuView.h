//
//  FGMenuView.h
//  FontGuide
//
//  Created by Mark Turner on 16/05/2013.
//  Copyright (c) 2013 Mark Turner. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FGUserState.h"

@class SVSegmentedControl;
@interface FGMenuView : UIView

@property (weak, nonatomic) IBOutlet UIButton *refreshButton;

@property (strong, nonatomic) SVSegmentedControl *layoutSegmentedControl;
@property (strong, nonatomic) SVSegmentedControl *reverseLayoutSwitch;

@property (strong, nonatomic) SVSegmentedControl *sortSegmentedControl;
@property (strong, nonatomic) SVSegmentedControl *sortReverseSwitch;

@property (nonatomic,assign) FGUserState * userState;

@end
