//
//  FGMenuView.m
//  FontGuide
//
//  Created by Mark Turner on 16/05/2013.
//  Copyright (c) 2013 Mark Turner. All rights reserved.
//

#import "FGMenuView.h"

#import "FGFontFamilyCell.h"
#import "FGFontManager.h"

#import "SVSegmentedControl.h"

@implementation FGMenuView

- (void)setUserState:(FGUserState *)userState
{
    switch (userState.layoutState.unsignedIntegerValue) {
        case FGFontFamilyCellLayoutJustifiedLeft:
            [self.layoutSegmentedControl setSelectedSegmentIndex:0 animated:FALSE];
            break;
        case FGFontFamilyCellLayoutJustifiedRight:
            [self.layoutSegmentedControl setSelectedSegmentIndex:1 animated:FALSE];
            break;
        default:
            break;
    }
    [self.reverseLayoutSwitch setSelectedSegmentIndex:userState.layoutBackwards.boolValue animated:FALSE];

    switch (userState.sortingState.unsignedIntegerValue) {
        case FGFontFamilySortAlpha:
            [self.sortSegmentedControl setSelectedSegmentIndex:0 animated:FALSE];
            break;
        case FGFontFamilySortCount:
            [self.sortSegmentedControl setSelectedSegmentIndex:1 animated:FALSE];
            break;
        case FGFontFamilySortSize:
            [self.sortSegmentedControl setSelectedSegmentIndex:2 animated:FALSE];
            break;
        default:
            break;
    }
    
    if (userState.sortingState.integerValue == FGFontFamilySortManual) {
        [self.sortSegmentedControl.thumb setHidden:TRUE];
        [self.sortReverseSwitch.thumb    setHidden:TRUE];
    } else {
        [self.sortSegmentedControl.thumb setHidden:FALSE];
        [self.sortReverseSwitch.thumb    setHidden:FALSE];
    }
    [self.sortReverseSwitch setSelectedSegmentIndex:userState.reverseSort.boolValue==FALSE?1:0 animated:FALSE];
}

- (void)awakeFromNib
{
	 _layoutSegmentedControl = [[SVSegmentedControl alloc] initWithSectionTitles:[NSArray arrayWithObjects:@"Left", @"Right", nil]];
    [_layoutSegmentedControl setCrossFadeLabelsOnDrag:TRUE];
    [_layoutSegmentedControl.thumb setTintColor:[UIColor colorWithRed:126.0/255.0 green:190/250.0 blue:219/255.0 alpha:1.0]];
    [_layoutSegmentedControl.thumb setShouldCastShadow:FALSE];
    [_layoutSegmentedControl setBackgroundTintColor:[UIColor colorWithRed:34.0/255.0 green:51.0/255.0 blue:67.0/255.0 alpha:1.0]];
    [_layoutSegmentedControl setSelectedSegmentIndex:1 animated:NO];
    [_layoutSegmentedControl setFrame:CGRectMake(14.0, 80.0, 170.0f, 40.0)];
	[self addSubview:_layoutSegmentedControl];

     _reverseLayoutSwitch = [[SVSegmentedControl alloc] initWithSectionTitles:[NSArray arrayWithObjects:@"Forwards",@"Backwards", nil]];
    [_reverseLayoutSwitch setCrossFadeLabelsOnDrag:TRUE];
    [_reverseLayoutSwitch.thumb setTintColor:[UIColor colorWithRed:126.0/255.0 green:190/250.0 blue:219/255.0 alpha:1.0]];
    [_reverseLayoutSwitch.thumb setShouldCastShadow:FALSE];
    [_reverseLayoutSwitch setBackgroundTintColor:[UIColor colorWithRed:34.0/255.0 green:51.0/255.0 blue:67.0/255.0 alpha:1.0]];
    [_reverseLayoutSwitch setSelectedSegmentIndex:1 animated:NO];
    [_reverseLayoutSwitch setFrame:CGRectMake(14.0, 165.0, 170.0f, 40.0)];
	[self addSubview:_reverseLayoutSwitch];

     _sortSegmentedControl = [[SVSegmentedControl alloc] initWithSectionTitles:[NSArray arrayWithObjects:@"Alpha",@"Count",@"Size", nil]];
    [_sortSegmentedControl setCrossFadeLabelsOnDrag:TRUE];
    [_sortSegmentedControl.thumb setTintColor:[UIColor colorWithRed:126.0/255.0 green:190/250.0 blue:219/255.0 alpha:1.0]];
    [_sortSegmentedControl.thumb setShouldCastShadow:FALSE];
    [_sortSegmentedControl setBackgroundTintColor:[UIColor colorWithRed:34.0/255.0 green:51.0/255.0 blue:67.0/255.0 alpha:1.0]];
    [_sortSegmentedControl setSelectedSegmentIndex:1 animated:NO];
    [_sortSegmentedControl setFrame:CGRectMake(14.0, 250.0, 170.0f, 40.0)];
	[self addSubview:_sortSegmentedControl];
    
     _sortReverseSwitch = [[SVSegmentedControl alloc] initWithSectionTitles:[NSArray arrayWithObjects:@"Asc",@"Des", nil]];
    [_sortReverseSwitch setCrossFadeLabelsOnDrag:TRUE];
    [_sortReverseSwitch.thumb setTintColor:[UIColor colorWithRed:126.0/255.0 green:190/250.0 blue:219/255.0 alpha:1.0]];
    [_sortReverseSwitch.thumb setShouldCastShadow:FALSE];
    [_sortReverseSwitch setBackgroundTintColor:[UIColor colorWithRed:34.0/255.0 green:51.0/255.0 blue:67.0/255.0 alpha:1.0]];
    [_sortReverseSwitch setSelectedSegmentIndex:1 animated:NO];
    [_sortReverseSwitch setFrame:CGRectMake(14.0, 335.0, 170.0f, 40.0)];
	[self addSubview:_sortReverseSwitch];
}

@end
