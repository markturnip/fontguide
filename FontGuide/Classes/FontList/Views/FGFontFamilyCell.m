//
//  FGFontFamilyCell.m
//  FontGuide
//
//  Created by Mark Turner on 15/05/2013.
//  Copyright (c) 2013 Mark Turner. All rights reserved.
//

#import "FGFontFamilyCell.h"
#import "FGFontFamily.h"
#import "FGUserState.h"

@implementation FGFontFamilyCell

- (void)setFontFamily:(FGFontFamily *)fontFamily
{
    FGUserState * userState = fontFamily.parent;
    if (userState != nil) {
        [self setLayoutState:fontFamily.parent.layoutState.integerValue];
        
        UIFont * font = [UIFont fontWithName:fontFamily.name size:fontFamily.size.unsignedIntegerValue];
        [self.fontLabel setFont:font];
        
        if (font == nil) NSLog(@"FONT NOT FOUND: %@",font);
        
        if (userState.layoutBackwards.boolValue) {
            NSString        * currentString = fontFamily.name;
            NSMutableString * reversedMutableString = [[NSMutableString alloc] initWithCapacity:currentString.length];
            [currentString enumerateSubstringsInRange:NSMakeRange(0, currentString.length)
                                              options:(NSStringEnumerationReverse|NSStringEnumerationByComposedCharacterSequences)
                                           usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
                [reversedMutableString appendString:substring];
            }];
            [self.fontLabel setText:reversedMutableString];
        } else {
            [self.fontLabel setText:fontFamily.name];
        }
    }
}

- (void)setLayoutState:(FGFontFamilyCellLayoutState)layoutState
{
    switch (layoutState) {
        case FGFontFamilyCellLayoutJustifiedLeft: {
            [self.fontLabel setTextAlignment:NSTextAlignmentLeft];
        }
            break;
        case FGFontFamilyCellLayoutJustifiedRight: {
            [self.fontLabel setTextAlignment:NSTextAlignmentRight];
        }
            break;
        default:
            break;
    }
}

@end
