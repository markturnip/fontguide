//
//  FGFontFamilyCell.h
//  FontGuide
//
//  Created by Mark Turner on 15/05/2013.
//  Copyright (c) 2013 Mark Turner. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    FGFontFamilyCellLayoutJustifiedLeft,  //Font name is justified left
    FGFontFamilyCellLayoutJustifiedRight, //Font name is justified left
} FGFontFamilyCellLayoutState;

@class FGFontFamily;

@interface FGFontFamilyCell : UITableViewCell
//Font justification state
@property (nonatomic,assign) FGFontFamilyCellLayoutState layoutState;
//Font names are character reversed
@property BOOL characterReversed;

@property (nonatomic,weak) IBOutlet UILabel * fontLabel;

@property (nonatomic,assign) FGFontFamily * fontFamily;

@end
